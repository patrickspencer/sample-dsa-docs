# Statistical and Mathematical Foundations #

## Possible data sets: ##

[US Censes Data](http://www2.census.gov/acs2013_1yr/summaryfile/)  
[FBI Crime Data](https://www.fbi.gov/about-us/cjis/ucr/crime-in-the-u.s)  
[Public weather datasets](http://apps.ecmwf.int/datasets/)  
[Youth risk behavioral study](http://www.cdc.gov/healthyyouth/yrbs/)  

### Fun datasets ###

[Twitter data](http://snap.stanford.edu/data/egonets-Twitter.html)  
[Top 2.5 million reddit posts](https://github.com/umbrae/reddit-top-2.5-million)  
[Pokemon database](https://pokeapi.co/)  
[Starwars api](https://swapi.co/)

### Set of datasets ###
[100 interesting data sets](http://rs.io/100-interesting-data-sets-for-statistics/)  
[List of fun datasets](http://koaning.io/fun-datasets.html)  
[Kaggle datasets](https://www.kaggle.com/datasets)  

## Analysis of variance ##

Box plot in R against violent crime statistics based on geographic
location versus something like age. Something like
[this](http://www.r-bloggers.com/performing-anova-test-in-r-results-and-interpretation/).

### Induction, deduction, and abduction ###

Deductive reasoning: (Sherlock holmes) If all begal shops are closed on
Sunday and BB Begal is a begal shop then BB begal is closed on Sundays.

Induction example: (Educated guess from the information given). Every swan
I've seen is white. Therefore I'm guessing all swans are white.

Abduction: (inferring event a as a consequence of b) Say the 8 ball is
traveling towards you in a game of pool. We may abduce that the cue ball
struck the 8 ball.

### Biases ###

### Independent and dependent variables ###

[Khan Academy: Dependent and Independendent variables](https://www.khanacademy.org/math/algebra/introduction-to-algebra/alg1-dependent-independent/v/dependent-and-independent-variables-exercise-example-1)

Examples:

* Height of rocket versus time
* Number of people invited to a dinner party versus how much food to
  make

## Modeling relationships ##

### Hypothesis testing ###

Examples:

* [Lady tasting tea](https://en.wikipedia.org/wiki/Statistical_hypothesis_testing#Lady_tasting_tea)
* [Courtroom trial](https://en.wikipedia.org/wiki/Statistical_hypothesis_testing#Courtroom_trial)

### Sampling ###

Youth risk behavioral study sampling

### ANOVA, Ordinary Least Squares regression ###

When do you use one or the other? 

* Regression: predicting a continuous outcome based on one or more
  *continuous* predictor values
* Anova: predicting a continuous outcome based on one or more
  *catagorical* predictor values

([source](http://www.pmean.com/08/RegressionAndAnova.html)) If
you are trying to predict the duration of breastfeeding in weeks using
mother's age as a predictor variable, then you would use a regression
model. If you are trying to predict the duration of breastfeeding in
weeks using mother's marital status (single, married, divorced,
widowed), the you would use an ANOVA model. If you are trying to predict
the duration of breastfeeding in weeks using prenatal smoking status
(smoked during pregnancy, did not smoke during pregnancy), then you
would use a two-sample t-test. If you added delivery type
(vaginal/c-section) to prenatal smoking status, then the two binary
predictor variables would be analyzed using an ANOVA model.

![least squares svg from wikipedia article](images/leastsquares.png)  
[Wikipedia article on least squares regression](https://en.wikipedia.org/wiki/Linear_least_squares_(mathematics))

### P-value, z-value, significance ###

Explain p-values and z-values with normal curve.

Introduce central limit theorem and take random samples of data (maybe
US census data)

[What is a z-score and a p-value](http://pro.arcgis.com/en/pro-app/tool-reference/spatial-statistics/what-is-a-z-score-what-is-a-p-value.htm)

![p-values and z-values](images/pvalues.png)

### T-test ###

[Wikipedia article on T-test](https://en.wikipedia.org/wiki/Student%27s_t-test)  
[Inferential statistics](https://esa21.kennesaw.edu/modules/basics/exercise3/3-8.htm)  
![t-test variability](images/t-test-variability.jpg)

### Chi-squares ###

[Wikipedia article on Chi-squared test](https://en.wikipedia.org/wiki/Chi-squared_test)  
![Chi-squared distributions from Wikipedia](images/chisquare-dist.png)  
[Chi square r tutorial/example](http://www.r-tutor.com/elementary-statistics/goodness-fit/chi-squared-test-independence)__

## Bivariate and multivariate regression ##

* Analyze the relationship between two variables, instead of one, in
  order to test simple hypothesis of association and causality.
  
[Little book of R for multivariate analysis](http://little-book-of-r-for-multivariate-analysis.readthedocs.io/en/latest/)

## Principle components analysis ##

[Visualizing PCR in R](http://www.r-bloggers.com/computing-and-visualizing-pca-in-r/)

Lab: PCA on iris dataset

![Guassian scatter PCA](images/GaussianScatterPCA.png)

## Parametric vs non-parametric ##

* Parametric: usually has a normal distirbution
* Non-parametric: one might define nonparametric statistical procedures
as a class of statistical procedures that do not rely on assumptions
about the shape or form of the probability distribution from which the
data were drawn

[Article which discribes parametric versus non-parametric](https://www.mayo.edu/mayo-edu-docs/center-for-translational-science-activities-documents/berd-5-6.pdf)

## What is a model ##

[Wikipedia article on statistical models](https://en.wikipedia.org/wiki/Statistical_model)  
[Statistical models in R (pdf)](https://www3.nd.edu/~steve/Rcourse/Lecture7v1.pdf)

### Rationale and logic behind models ###

### Model terms ###

* Explanatory variables (look in "Statistical model in R (PDF)")
* Response variable: continuous proportion, count, binary, time-at-death

### Fitting models to data ###

### Types of models ###

* Linear regression
* non-linear models

##  Understanding and managing the randomness within models ##

### Statistical adjustment ###

### Model fitting ###

### Evaluating models ###

### Decomposing variance ###

## Multivariate regression in R ##

[Examples of multivariate regression in R](http://www.statmethods.net/stats/regression.html)

## Clustering in R ##

[Examples of clustering in R](http://www.statmethods.net/advstats/cluster.html)

## Monte Carlo techniques ##

[Wikipedia article on the Monte Carlo method](https://en.wikipedia.org/wiki/Monte_Carlo_method)  
[Brilliant: Monte Carlo Method](https://brilliant.org/wiki/monte-carlo/)
[Examples of Monte Carlo method in R](http://www.stat.ufl.edu/archived/casella/ShortCourse/MCMC-UseR.pdf)  
[Monte Carlo simulations in R (Examples)](https://web.stanford.edu/class/bios221/labs/simulation/Lab_3_simulation.html)  
[Youtube video of Monte Carlo simulation](https://www.youtube.com/watch?v=Lv-ofCiL5hI)

Examples (From Brilliant link above, python code included)

* Write a function that uses Monte Carlo to simulate the the probability
of getting a pair of 6's within a twenty-four rolls of a pair of dice.
* Estimate the value of pi

## Different styles of modeling ##

### Regression - linear, logistic, multinomial, multivariate ###

### Bootstrap ###

### Categories of machine learning, linear models ###

### Baysian techniques ###

## Factor analysis ##

![Kernal Machine (From Wikipedia)](images/Kernel_Machine.png)

## Dimension reduction ##

